# Docker configuration for transport DB

Usage:

```
docker-compose up
```

The first start will take a while, as it will create the docker image locally, and installs the transport-schema at container startup.

Port 5432 is forwarded to 5432 on the container host. Connect to database 'transport_db', with user 'qrious'. 
