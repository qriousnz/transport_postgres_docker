FROM postgres

# replace httpredir with a concrete mirror, without this the postgis install sometimes fails
RUN apt-get update
RUN apt-get install curl -y
RUN sed -i "s/httpredir.debian.org/`curl -s -D - http://httpredir.debian.org/demo/debian/ | awk '/^Link:/ { print $2 }' | sed -e 's@<http://\(.*\)/debian/>;@\1@g'`/" /etc/apt/sources.list

# install postgis
RUN apt-get clean
RUN apt-get update
RUN apt-get install postgis -y

# set dbname and user
#ENV POSTGRES_PASSWORD secret
ENV POSTGRES_USER qrious
ENV POSTGRES_DB transport_db

# install schema
RUN apt-get install git -y
RUN git clone https://johannesvogt01@bitbucket.org/qriousnz/transport_schema_flyway.git
RUN apt-get install maven -y

COPY run-flyway.sh /docker-entrypoint-initdb.d/